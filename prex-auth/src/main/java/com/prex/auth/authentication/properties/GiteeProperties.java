package com.prex.auth.authentication.properties;

import lombok.Data;

/**
 * @Classname GiteeProperties
 * @Description 码云第三方登录配置
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-07-08 21:49
 * @Version 1.0
 */
@Data
public class GiteeProperties extends SocialProperties {

    private String providerId = "gitee";
}
